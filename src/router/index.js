import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'

import ListCustomers from '@/components/Customer/List.vue'
import ShowCustomer from '@/components/Customer/Show.vue'
import EditCustomer from '@/components/Customer/Edit.vue'
import AddCustomer from '@/components/Customer/Add.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/customers'
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/customers',
      name: 'Customers',
      component: ListCustomers
    },
    {
      path: '/customer/:id',
      name: 'ShowCustomer',
      component: ShowCustomer
    },
    {
      path: '/customer/:id/edit',
      name: 'EditCustomer',
      component: EditCustomer
    },
    {
      path: '/customer/add',
      name: 'AddCustomer',
      component: AddCustomer
    }
  ]
})
