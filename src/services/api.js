import axios from 'axios'

export default class ApiService {
  constructor () {
    this.baseUri = 'http://localhost:1234/api/'
  }

  addCustomer (customer) {
  }

  updateCustomer (customer) {

  }

  fetchCustomers (page = 1) {
  }

  get (path) {
    return axios.get(this.baseUri + path)
  }
}
