import ApiService from './api'
import MockService from './mock'

// Export as singletons, if this is getting more complex by time we should think about using a DI library
export let apiService = new ApiService()
export let mockService = new MockService()
