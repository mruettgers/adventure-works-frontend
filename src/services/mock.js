import faker from 'faker'

export default class MockService {
  constructor () {
    this.total = 200
    this.itemsPerPage = 20
    this.customers = this.fakeCustomers(this.total)
  }

  init () {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          states: [],
          countries: []
        })
      }, 3000)
    })
  }

  fetchCustomers (page = 1) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          meta: {
            page,
            itemsPerPage: this.itemsPerPage,
            total: this.total
          },
          items: this.customers.slice((page - 1) * this.itemsPerPage, page * this.itemsPerPage)
        })
      }, 500)
    })
  }

  fetchCustomer (customerId) {
    const parsedCustomerId = parseInt(customerId)
    return new Promise((resolve) => {
      const customer = this.customers.find(c => c.CustomerID === parsedCustomerId)
      if (!customer) {
        throw new Error(`Could not find customer with ID ${customerId}.`)
      }
      const orderItems = this.fakeSomeOrders()
      setTimeout(() => {
        resolve({
          meta: {},
          item: {
            ...customer,
            orders: {
              items: orderItems,
              meta: {
                page: 1,
                itemsPerPage: null,
                total: orderItems.length
              }
            }
          }
        })
      }, 500)
    })
  }

  fakeSomeOrders () {
    return Array(faker.random.number({min: 0, max: 15})).fill().map((_, i) => {
      return {
        OrderHeaderID: 1,
        OrderNumber: 'Order # ' + (i + 1),
        Price: faker.commerce.price(),
        OrderDate: faker.date.past(3)
      }
    })
  }

  fakeCustomers (amount) {
    return Array(amount).fill().map((_, i) => {
      return {
        CustomerID: i + 1,
        BusinessEntityID: 11000 + i + 1,
        FirstName: faker.name.firstName(),
        LastName: faker.name.lastName(),
        EmailAddresses: Array(faker.random.number({min: 0, max: 3})).fill().map((_, i) => {
          return {
            EmailAddressID: i + 1,
            EmailAddress: faker.internet.email()
          }
        }),
        Addresses: Array(faker.random.number({min: 1, max: 2})).fill().map((_, i) => {
          return {
            Address: faker.address.streetAddress()
          }
        })
      }
    })
  }
}
