import Vue from 'vue'
import Vuex from 'vuex'
import { mockService } from '../services'

Vue.use(Vuex)

const state = {
  initialized: false,
  shared: {
    countries: [],
    states: []
  },
  customers: {
    loading: false,
    itemsPerPage: null,
    total: null,
    items: []
  },
  customer: {
    loading: false,
    item: null,
    meta: null
  }
}

const mutations = {
  init (state, data) {
    const {countries, states} = data
    state.countries = countries
    state.states = states
  },
  setInitialized (state, initialized) {
    state.initialized = initialized
  },
  setCustomersLoading (state, loading) {
    state.customers.loading = loading
  },
  setCustomers (state, {items, meta: {itemsPerPage, total}}) {
    state.customers.items = items
    state.customers.total = total
    state.customers.itemsPerPage = itemsPerPage
  },
  setCustomerLoading (state, loading) {
    state.customer.loading = loading
  },
  setCustomer (state, {item}) {
    state.customer.item = item
  }
}

const actions = {
  init (context) {
    context.commit('setInitialized', false)
    mockService
      .init()
      .then(({states = [], countries = []}) => {
        context.commit('init', {states, countries})
        context.commit('setInitialized', true)
      })
  },
  fetchCustomers (context, page) {
    context.commit('setCustomersLoading', true)
    mockService
      .fetchCustomers(page)
      .then((result) => {
        context.commit('setCustomers', result)
        context.commit('setCustomersLoading', false)
      })
  },
  fetchCustomer (context, id) {
    context.commit('setCustomerLoading', true)
    mockService
      .fetchCustomer(id)
      .then((result) => {
        context.commit('setCustomer', result)
        context.commit('setCustomerLoading', false)
      })
  },
  updateCustomer (context, customer) {
  },
  addCustomer (context, customer) {
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions
})

export default store
